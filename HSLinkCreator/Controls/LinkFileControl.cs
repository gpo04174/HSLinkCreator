﻿using HSLinkCreator.Core;
using HSLinkCreator.Language;
using Ionic.Utils;
using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HSLinkCreator.Controls
{
    public partial class LinkFileControl : UserControl
    {
        public LinkFileControl()
        {
            InitializeComponent();
            LanguageManager.LanguageChanged += (s, e) => UpdateLanguage();
            //FolderBrowserDialogEx.MakeSpecialBrowser(CSIDL.DRIVES).ShowDialog();
        }

        private void LinkFileControl_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 1;
            listView1.ApplyTheme = true;

            UpdateLanguage();
        }

        public LinkFile[] Links
        {
            get
            {
                LinkFile[] Links = new LinkFile[listView1.Items.Count];
                for (int i = 0; i < Links.Length; i++)
                    Links[i] = listView1.Items[i].Tag as LinkFile;
                return Links;
            }
        }

        private void btnDestination_Click(object sender, EventArgs e)
        {
            //dialog.RootFolder = Environment.SpecialFolder.Desktop;
            FolderBrowserDialogEx dialog = FolderBrowserDialogEx.LocalBrowser( LanguageManager.Language["STR_FRM_LINKFILE_DLG_OPEN"]);
            dialog.SelectedPath = textBox1.Text;
            if (dialog.ShowDialog(this) == DialogResult.OK)
                textBox1.Text = dialog.SelectedPath;
        }

        OpenFileDialog dialog1 = new OpenFileDialog()
        {
            Multiselect = true,
            DefaultExt = "*.*",
            Filter = LanguageManager.Language["STR_FRM_LINKFILE_TSM_EXT_ALL"] + "|*.*"
        };
        private async void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show(LanguageManager.Language["STR_FRM_LINKFILE_DLG_DIROPEN"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnDestination_Click(sender, e);
            }

            if (!string.IsNullOrEmpty(textBox1.Text) && dialog1.ShowDialog() == DialogResult.OK)
                await Add(true, dialog1.FileNames);
        }

        private void btnClear_Click(object sender, EventArgs e) { if (MessageBox.Show(LanguageManager.Language["STR_FRM_LINKFILE_MSG_DELETEALL"], Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.OK) listView1.Items.Clear(); }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) deleteToolStripMenuItem_Click(sender, e);
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LinkFile[] linkf = new LinkFile[listView1.SelectedItems.Count];
            ListViewItem[] itemf = new ListViewItem[linkf.Length];
            for (int i = 0; i < linkf.Length; i++)
            {
                itemf[i] = listView1.SelectedItems[i];
                linkf[i] = itemf[i].Tag as LinkFile;
            }

            frmEdit rename = new frmEdit(linkf);
            if (rename.ShowDialog() == DialogResult.OK)
                for (int i = 0; i < itemf.Length; i++) Edit(itemf[i], rename.Link[i] as LinkFile);

        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            deleteToolStripMenuItem.Enabled = listView1.SelectedItems.Count > 0;
            editToolStripMenuItem.Enabled = listView1.SelectedItems.Count == 1;
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = listView1.SelectedItems.Count - 1; i >= 0; i--)
                listView1.SelectedItems[i].Remove();
        }

        #region Core
        LinkProgressAdd InitProgress(int TotalCount = 0)
        {
            LinkProgressAdd p = new LinkProgressAdd();
            p.DoneClicked += (e) => panel1.Enabled = true;
            p.Size = new System.Drawing.Size(350, 100);
            int x = (Width - p.Width) / 2;
            int y = (Height - p.Height) / 2;
            p.Location = new System.Drawing.Point(x, y);
            this.SizeChanged += (o, e) =>
            {
                if (!p.IsDisposed)
                {
                    x = (Width - p.Width) / 2;
                    y = (Height - p.Height) / 2;
                    p.Location = new System.Drawing.Point(x, y);
                }
            };
            p.TotalCount = TotalCount;
            p.BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(p);
            p.BringToFront();
            return p;
        }

        ListViewItem Make(string File, LinkKind kind)
        {
            string Orig = File.Remove(File.IndexOf("\\"));
            string Dest = textBox1.Text.Remove(textBox1.Text.IndexOf("\\"));
            
            if (kind == LinkKind.Special && Orig.ToUpper() != Dest.ToUpper()) kind = LinkKind.SymLink;
            LinkFile link = new LinkFile(File, textBox1.Text + (textBox1.Text[textBox1.Text.Length - 1] == '\\' ? "" : "\\") + Path.GetFileName(File), kind);

            string dest = link.DestinationPath + (kind == LinkKind.Shortcut ? ".lnk" : null);
            ListViewItem li = new ListViewItem(Path.GetFileName(dest));

            if (link.Kind == LinkKind.Shortcut) li.SubItems.Add(LanguageManager.Language["STR_SHORTCUT"]);
            else if (link.Kind == LinkKind.SymLink) li.SubItems.Add(LanguageManager.Language["STR_SYMLINK"]);
            else li.SubItems.Add(LanguageManager.Language["STR_HARDLINK"]);

            li.SubItems.Add(link.OriginalPath);
            li.SubItems.Add(dest);
            li.Tag = link;
            return li;
        }
        ListViewItem[] Makes(params string[] Files)
        {
            ListViewItem[] items = new ListViewItem[Files.Length];
            LinkKind kind = GetLinkKind();
            for (int i = 0; i < Files.Length; i++) items[i] = Make(Files[i], kind);
            return items;
        }
        async Task Add(bool UseBuffer, params string[] Files)
        {
            int cnt = UseBuffer ? Program.BufferCnt : 1;
            int j = 1;
            ListViewItem[] items = new ListViewItem[cnt];

            panel1.Enabled = false;
            LinkProgressAdd p = InitProgress(Files.Length);
            try
            {
                LinkKind kind = GetLinkKind();
                for (int i = 0; i < Files.Length && !p.IsCancel; i++)
                {
                    p.CurrentCount++;
                    try
                    {
                        items[j - 1] = Make(Files[i], kind);
                        p.Added++;
                        if (j == items.Length)
                        {
                            j = 0;
                            listView1.Items.AddRange(items);
                            await Task.Delay(1);
                        }
                        j++;
                    }
                    catch(Exception ex) { p.Fail++; await Task.Delay(1); }
                    p.Update();
                }
                for (int i = 0; i < j - 1; i++)
                {
                    listView1.Items.Add(items[i]);
                    await Task.Delay(1);
                }
            }
            finally { p.Done(true); }//Controls.Remove(p); p.Dispose();
        }
        void Edit(int index, LinkFile link) { Edit(listView1.Items[index], link); }
        void Edit(ListViewItem li, LinkFile link)
        {
            li.Tag = link;
            li.SubItems[0].Text = Path.GetFileName(link.DestinationPath);
            li.SubItems[2].Text = link.OriginalPath;
            li.SubItems[3].Text = link.DestinationPath;

            if (link.Kind == LinkKind.Shortcut) li.SubItems[1].Text = LanguageManager.Language["STR_SHORTCUT"];
            else if (link.Kind == LinkKind.SymLink) li.SubItems[1].Text = LanguageManager.Language["STR_SYMLINK"];
            else li.SubItems[1].Text = LanguageManager.Language["STR_HARDLINK"];
        }
        LinkKind GetLinkKind()
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0: return LinkKind.Shortcut;
                case 2: return LinkKind.Special;
                default: return LinkKind.SymLink;
            }
        }
        int SetLinkKind(LinkKind kind)
        {
            switch (kind)
            {
                case LinkKind.Shortcut: return 0;
                case LinkKind.Special: return 2;
                default: return 1;
            }
        }

        public void UpdateLanguage()
        {
            lblPath.Text = LanguageManager.Language["STR_FRM_LBL_PATH"];
            lblType.Text = LanguageManager.Language["STR_FRM_LBL_TYPE"];
            btnAdd.Text = LanguageManager.Language["STR_FRM_BTN_ADD"];
            btnClear.Text = LanguageManager.Language["STR_FRM_BTN_CLRALL"];
            comboBox1.Items[0] = LanguageManager.Language["STR_SHORTCUT"];
            comboBox1.Items[1] = LanguageManager.Language["STR_SYMLINK"];
            comboBox1.Items[2] = LanguageManager.Language["STR_HARDLINK"];
            columnHeader1.Text = LanguageManager.Language["STR_FRM_TAB_NAME"];
            columnHeader2.Text = LanguageManager.Language["STR_FRM_TAB_TYPE"];
            columnHeader3.Text = LanguageManager.Language["STR_FRM_TAB_DEST"];
            columnHeader4.Text = LanguageManager.Language["STR_FRM_TAB_ORIG"];
            editToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_EDIT"];
            deleteToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_DELETE"];

            //dialog.Description = LanguageManager.Language["STR_FRM_LINKFILE_DLG_OPEN"];

            for (int i = 0; i < listView1.Items.Count; i++)
            {
                LinkFile link = listView1.Items[i].Tag as LinkFile;
                if (link.Kind == LinkKind.Shortcut) listView1.Items[i].SubItems[1].Text = LanguageManager.Language["STR_SHORTCUT"];
                else if (link.Kind == LinkKind.SymLink) listView1.Items[i].SubItems[1].Text = LanguageManager.Language["STR_SYMLINK"];
                else listView1.Items[i].SubItems[1].Text = LanguageManager.Language["STR_HARDLINK"];
            }
        }
        #endregion
        
        private async void listView1_DragDrop(object sender, DragEventArgs e)
        {
            if(string.IsNullOrEmpty(textBox1.Text))
            {
                MessageBox.Show(LanguageManager.Language["STR_FRM_LINKFILE_DLG_DIROPEN"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnDestination_Click(sender, e);
            }
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                panel1.Enabled = false;
                LinkProgressAdd p = InitProgress();
                LinkKind kind = GetLinkKind();
                try
                {
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    p.TotalCount = files.Length;

                    ListViewItem[] buffer = new ListViewItem[Program.BufferCnt];
                    int j = 1;
                    for (int i = 0; i < files.Length; i++)
                    {
                        p.CurrentCount++;
                        try
                        {
                            if (File.Exists(files[i]))
                            {
                                buffer[j - 1] = Make(files[i], kind);
                                p.Added++;
                                if (j == buffer.Length)
                                {
                                    j = 0;
                                    listView1.Items.AddRange(buffer);
                                    await Task.Delay(1);
                                }
                                j++;
                            }
                            else p.Pass++;
                        }
                        catch { p.Fail++; }
                        p.Update();
                    }
                    for (int i = 0; i < j - 1; i++)
                    {
                        listView1.Items.Add(buffer[i]);
                        await Task.Delay(1);
                    }
                }
                finally { p.Done(false); }
            }
            else MessageBox.Show(LanguageManager.Language["STR_FRM_LINKDIR_DLG_ORIG"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void listView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        bool ShowMessaged = false;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox1.SelectedIndex == 2 && !ShowMessaged)
            {
                MessageBox.Show(LanguageManager.Language["STR_MSG_HARDLINK_WARN"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                ShowMessaged = true;
            }
        }
    }
}
