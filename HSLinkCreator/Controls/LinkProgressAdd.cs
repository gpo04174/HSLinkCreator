﻿using HSLinkCreator.Language;
using System;
using System.Windows.Forms;

namespace HSLinkCreator.Controls
{
    public partial class LinkProgressAdd : UserControl
    {
        //public delegate void ProgressEventHandler(LinkProgressAdd sender, ProgressEventArgs e);
        //public delegate void AddingEventHandler(LinkProgressAdd sender, string Path);
        //public delegate void CompleteEventHandler(LinkProgressAdd sender, bool Cancel);
        public delegate void DoneClickedEventHandler(object sender);
        public LinkProgressAdd()
        {
            InitializeComponent();
            //LanguageManager.LanguageChanged += LanguageManager_LanguageChanged;
        }


        //public event AddingEventHandler Adding;
        //public event ProgressEventHandler Progress;
        //public event CompleteEventHandler Complete;
        public event DoneClickedEventHandler DoneClicked;
        public new void Update()
        {
            Text = string.Format("{0} [{1} / {2}] ({3:0.00}%)\n\n{4}: [{5}: {6} / {7}: {8} / {9}: {10}]",
                   LanguageManager.Language["STR_FRM_PROGRESS_ADDING"], CurrentCount, TotalCount, Percent,
                   LanguageManager.Language["STR_FRM_PROGRESS_RESULT"], LanguageManager.Language["STR_FRM_PROGRESS_ADD"], Added, LanguageManager.Language["STR_FRM_PROGRESS_FAIL"], Fail, LanguageManager.Language["STR_FRM_PROGRESS_PASS"], Pass);

            if(progressBar1.Maximum != TotalCount) progressBar1.Maximum = TotalCount;
            progressBar1.Value = CurrentCount;
            progressBar1.Update();
        }
        public override string Text { get { return label1.Text; } set { label1.Text = value; } }
        public int TotalCount { get; internal set; }
        public int CurrentCount { get; internal set; }
        public int Added { get; internal set; }
        public int Fail { get; internal set; }
        public int Pass { get; internal set; }
        public float Percent { get { return ((float)CurrentCount / TotalCount) * 100; } }

        public void Done(bool AutoClick)
        {
            if (AutoClick) btnCancel_Click(this, null);
            else
            {
                btnCancel.Tag = "STR_BTN_OK";
                btnCancel.Text = LanguageManager.Language[btnCancel.Tag.ToString()];
                Text = string.Format("{0} ({1})\n\n [{2}: {3} / {4}: {5} / {6}: {7}]",
                        LanguageManager.Language["STR_MAIN_STAT_COMPLETE"], LanguageManager.Language["STR_FRM_PROGRESS_TOTAL"].Replace("%s", TotalCount.ToString()),
                        LanguageManager.Language["STR_FRM_PROGRESS_RESULT"], Added, LanguageManager.Language["STR_FRM_PROGRESS_FAIL"], Fail, LanguageManager.Language["STR_FRM_PROGRESS_PASS"], Pass);
            }
        }

        public bool IsCancel { get; set; }

        public void Start() {timer1.Start();}
        public void Stop() { timer1.Stop(); }
        public void Reset(int TotalCount)
        {
            IsCancel = false;
            TotalCount = CurrentCount = Added = Fail = Pass = 0;
            Update();
        }

        private void LinkProgressAdd_Load(object sender, EventArgs e)
        {
            LanguageManager_LanguageChanged(null, null);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            IsCancel = true;
            Dispose();
            if (DoneClicked != null) try { DoneClicked(this); } catch { }
        }

        private void LanguageManager_LanguageChanged(object sender, EventArgs e)
        {
            btnCancel.Text = LanguageManager.Language[btnCancel.Tag.ToString()];
            Update();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Update();
        }
    }
}
