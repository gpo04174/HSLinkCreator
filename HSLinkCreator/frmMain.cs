﻿using HSLinkCreator.Controls;
using HSLinkCreator.Core;
using HSLinkCreator.Language;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HSLinkCreator
{
    public partial class frmMain : Form
    {
        public static string NowTime() { DateTime dt = DateTime.Now; return dt.ToString(@"tt hh:mm:ss", CultureInfo.CurrentUICulture); }//return string.Format("{0:D2}:{1:D2}:{2:D2}.{3:D3}", dt.Hour, dt.Minute, dt.Second, dt.Millisecond); }

        bool IsBusy = false;
        MakeLink make;
        public frmMain()
        {
            InitializeComponent();
            make = new MakeLinkEXE(this);
            //make.Cancel += (o, m, p) => Stop();
            make.Complete += (o, e) =>
            {
                Done();
                lblStatus.Tag = "STR_MAIN_STAT_COMPLETE";
                lblStatus.Text = LanguageManager.Language["STR_MAIN_STAT_COMPLETE"];
            };
            make.Progressed += (o, e) =>
            {
                ListViewItem li = Find(e.Link);
                if (li != null)
                {
                    LastIndex = li.Index;
                    if (e.Result == Result.Success && chkAutoClear.Checked) li.Remove();
                    else
                    {
                        string result = null;
                        switch (e.Result)
                        {
                            case Result.Success: result = "STR_RESULT_SUCCESS"; break;
                            case Result.Fail: result = "STR_RESULT_FAIL"; break;
                            case Result.Starting: result = "STR_RESULT_STARTING"; break;
                            case Result.Unknown: result = "STR_RESULT_UNKNOWN"; break;
                            default: result = e.Result.ToString(); break;
                        }
                        li.SubItems[0].Tag = result;
                        li.SubItems[0].Text = LanguageManager.Language[result];
                    }
                    if (make.TotalCount != progressBar1.Maximum) progressBar1.Maximum = make.TotalCount;
                    progressBar1.Value = make.CurrentCount;
                    //index++;
                }
            };
            make.Logging += (o, e) =>
            {
                string d;
                switch (e.Result)
                {
                    case Result.Fail: d = "{E}"; break;
                    case Result.Starting: d = "{M}"; break;
                    case Result.Success: d = "{S}"; break;
                    case Result.Unknown: d = "{U}"; break;
                    default: d = "{" + e.Result.ToString() + "}"; break;
                }
                string k = null;
                switch (e.Link.Kind)
                {
                    case LinkKind.Shortcut: k = LanguageManager.Language["STR_SHORTCUT"]; break;
                    case LinkKind.SymLink: k = LanguageManager.Language["STR_SYMLINK"]; break;
                    case LinkKind.Special:
                        LinkDirectory link = e.Link as LinkDirectory;
                        if (link != null)
                        {
                            if (link.IsOnlyFile) k = LanguageManager.Language["STR_HARDLINK"];
                            else k = LanguageManager.Language["STR_JUNCLINK"];
                        }
                        else k = LanguageManager.Language["STR_HARDLINK"]; break;
                }
                txtLog.AppendText(
                    string.IsNullOrEmpty(e.Message) ?
                        string.Format("{0}: {1} [{2}] {3} -> {4}\r\n", NowTime(), d, k, e.ToString(), e.Link.DestinationPath) :
                        string.Format("{0}: {1} [{2}] {3} ({4}) -> {5}\r\n", NowTime(), d, k, e.ToString(), e.Message, e.Link.DestinationPath));

                lblStatusMsg.Text = string.Format("{1} / {2} ({3:0.00}%) [{0}] ", e.ToString(), make.CurrentCount, make.TotalCount, make.Percent);
            };
            chkStoreAttributes.CheckedChanged += (o, e) => make.ApplyProperty = chkStoreAttributes.Checked;
            chkStoreDate.CheckedChanged += (o, e) => make.ApplyDate = chkStoreDate.Checked;
            chkLinkReplace.CheckedChanged += (o, e) => make.LinkReplace = chkStoreDate.Checked;
            chkLogWrap.CheckedChanged += (o, e) => txtLog.WordWrap = chkLogWrap.Checked;
            chkLogScroll.CheckedChanged += (o, e) => txtLog.AutoScroll = chkLogWrap.Checked;
            chkAutoClear.CheckedChanged += (o, e) => btnClearComplete.Enabled = !chkAutoClear.Checked;

            selectAllToolStripMenuItem.Click += (o, e) => { for (int i = 0; i < hsListView1.Items.Count; i++) hsListView1.Items[i].Selected = true; };
            selectNoneToolStripMenuItem.Click += (o, e) => { for (int i = 0; i < hsListView1.Items.Count; i++) hsListView1.Items[i].Selected = false; };
            selectReverseToolStripMenuItem.Click += (o, e) => { for (int i = 0; i < hsListView1.Items.Count; i++) hsListView1.Items[i].Selected = !hsListView1.Items[i].Selected; };
            checkAllToolStripMenuItem.Click += (o, e) => { for (int i = 0; i < hsListView1.Items.Count; i++) hsListView1.Items[i].Checked = true; };
            checkNoneToolStripMenuItem.Click += (o, e) => { for (int i = 0; i < hsListView1.Items.Count; i++) hsListView1.Items[i].Checked = false; };
            checkReverseToolStripMenuItem.Click += (o, e) => { for (int i = 0; i < hsListView1.Items.Count; i++) hsListView1.Items[i].Checked = !hsListView1.Items[i].Checked; };


            string process = Process.GetCurrentProcess().Modules[0].FileName;
            process = process.Remove(process.LastIndexOf("\\") + 1);
            SettingsPath = process + "Settings.ini";
        }

        private void CheckAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        void Abort()
        {
            Done();
            lblStatus.Tag = "STR_MAIN_STAT_ABORT";
            lblStatus.Text = LanguageManager.Language["STR_MAIN_STAT_ABORT"];
        }
        void Done()
        {
            timer1.Stop();
            IsBusy = false;
            progressBar1.Visible = lblStatusMsg.Visible = false;
            btnAddFile.Enabled = btnAddDirectory.Enabled = true;
            btnMake.Tag = "STR_MAIN_CREATE";
            btnMake.Text = LanguageManager.Language["STR_MAIN_CREATE"];
        }

        LinkProgressAdd InitProgress(int TotalCount = 0)
        {
            LinkProgressAdd p = new LinkProgressAdd();
            p.DoneClicked += (e) => tabControl1.Enabled = true;
            p.Size = new System.Drawing.Size(350, 100);
            int x = (Width - p.Width) / 2;
            int y = (Height - p.Height) / 2;
            p.Location = new System.Drawing.Point(x, y);
            p.TotalCount = TotalCount;
            p.BorderStyle = BorderStyle.FixedSingle;
            Controls.Add(p);
            p.BringToFront();
            return p;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
             hsListView1.ApplyTheme = true;
            aboutLinkLabel.LinkClicked += (o, e1) => Process.Start("https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/mklink");
            viewSourceCodeToolStripMenuItem.Click += (o, e1) => Process.Start("https://gitlab.com/gpo04174/HSLinkCreator");
            btnLogClear.Click += (o, e1) => txtLog.Clear();
            toolStripComboBox1.SelectedIndexChanged += (o, e1) =>
            {
                if (toolStripComboBox1.SelectedIndex == 1) LanguageManager.Language = new LanguageManager(Properties.Resources.ko_kr, "한국어");
                else LanguageManager.Language = LanguageManager.GetDefault();
                SaveSettings();
            };

            LanguageManager.LanguageChanged += (s, e1) => UpdateLanguage();
            LoadSettings();

            if (toolStripComboBox1.SelectedIndex < 0) toolStripComboBox1.SelectedIndex = 0;
        }

        int LastIndex = 0;
        CancellationTokenSource token = new CancellationTokenSource(-1);
        private async void btnMake_Click(object sender, EventArgs e)
        {
            if (!IsBusy)
            {
                //index = 0;
                if (hsListView1.Items.Count > 0)
                {
                    IsBusy = true;
                    progressBar1.Value = 0;
                    progressBar1.Visible = lblStatusMsg.Visible = true;
                    lblStatus.Tag = "STR_MAIN_STAT_START";
                    lblStatus.Text = LanguageManager.Language["STR_MAIN_STAT_START"];
                    btnMake.Tag = "STR_MAIN_ABORT";
                    btnMake.Text = LanguageManager.Language["STR_MAIN_ABORT"];
                    btnAddFile.Enabled = btnAddDirectory.Enabled = false;
                    make.LinkReplace = chkLinkReplace.Checked;
                    timer1.Start();

                    await Task.Run(() =>
                    {
                        Invoke(new System.Action(() =>
                        {
                            Link[] links = null;
                            if (hsListView1.CheckedItems.Count > 0)
                            {
                                links = new Link[hsListView1.CheckedItems.Count];
                                for (int i = 0; i < links.Length; i++) { links[i] = hsListView1.CheckedItems[i].Tag as Link; hsListView1.CheckedItems[i].Text = null; }
                            }
                            else
                            {
                                links = new Link[hsListView1.Items.Count];
                                for (int i = 0; i < links.Length; i++) { links[i] = hsListView1.Items[i].Tag as Link; hsListView1.Items[i].Text = null; }
                            }
                            make.Start(links);
                        }));
                    }, token.Token);
                }
                else MessageBox.Show(LanguageManager.Language["STR_MAIN_MSG_ADDITEM"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                token.Cancel();
                timer1.Stop();
                make.Stop();
                hsListView1.Items[LastIndex].SubItems[0].Tag = "STR_MAIN_STAT_ABORT";
                hsListView1.Items[LastIndex].SubItems[0].Text = LanguageManager.Language["STR_MAIN_STAT_ABORT"];
            }
        }

        ListViewItem Make(LinkFile link)
        {
            ListViewItem li = new ListViewItem("") { Tag = link, Checked = true };

            string dest = link.DestinationPath + (link.Kind == LinkKind.Shortcut ? ".lnk" : null);
            li.SubItems.Add(Path.GetFileName(dest));
            li.SubItems.Add(LanguageManager.Language["STR_FILE"]);

            if (link.Kind == LinkKind.Shortcut) li.SubItems.Add(LanguageManager.Language["STR_SHORTCUT"]);
            else if (link.Kind == LinkKind.SymLink) li.SubItems.Add(LanguageManager.Language["STR_SYMLINK"]);
            else li.SubItems.Add(LanguageManager.Language["STR_HARDLINK"]);

            li.SubItems.Add(dest);
            li.SubItems.Add(link.OriginalPath);
            return li;
        }
        ListViewItem Make(LinkDirectory link)
        {
            ListViewItem li = new ListViewItem("") { Tag = link, Checked = true };

            string dest = link.DestinationPath + (link.Kind == LinkKind.Shortcut ? ".lnk" : null);
            li.SubItems.Add(Path.GetFileName(dest[dest.Length - 1] == '\\' ? dest.Remove(dest.Length - 1) : dest));
            li.SubItems.Add(LanguageManager.Language["STR_DIR"]);

            if (link.Kind == LinkKind.Shortcut) li.SubItems.Add(LanguageManager.Language["STR_SHORTCUT"]);
            else if (link.Kind == LinkKind.SymLink) li.SubItems.Add(LanguageManager.Language["STR_SYMLINK"]);
            else
            {
                if (link.IsOnlyFile) li.SubItems.Add(LanguageManager.Language["STR_HARDLINK"]);
                else li.SubItems.Add(LanguageManager.Language["STR_JUNCLINK"]);
            }

            if (link.IsOnlyFile) li.SubItems[3].Text += string.Format("({0})", link.Recursive ? "●" : "○");
            li.SubItems.Add(dest);
            li.SubItems.Add(link.OriginalPath);
            return li;
        }

        private async void btnAddFile_Click(object sender, EventArgs e)
        {
            frmLinkFile dialog_file = new frmLinkFile();
            DialogResult dr = dialog_file.ShowDialog();
            if (dr == DialogResult.OK)
            {
                tabControl1.Enabled = false;
                LinkProgressAdd p = InitProgress();
                try
                {
                    p.TotalCount = dialog_file.Links.Length;

                    ListViewItem[] buffer = new ListViewItem[Program.BufferCnt];
                    int j = 1;
                    for (int i = 0; i < dialog_file.Links.Length; i++)
                    {
                        try
                        {
                            p.CurrentCount++;
                            p.Added++;
                            LinkFile link = dialog_file.Links[i];
                            buffer[j - 1] = Make(link);

                            if (j == buffer.Length)
                            {
                                j = 0;
                                hsListView1.Items.AddRange(buffer);
                                await Task.Delay(1);
                            }
                            j++;
                        }
                        catch (Exception ex) { p.Fail++; }
                        p.Update();
                    }
                    for (int i = 0; i < j - 1; i++)
                    {
                        hsListView1.Items.Add(buffer[i]);
                        await Task.Delay(1);
                    }
                }
                finally { p.Done(true); }
            }
        }

        private async void btnAddDirectory_Click(object sender, EventArgs e)
        {
            frmLinkDirectory dialog_directory = new frmLinkDirectory();
            if (dialog_directory.ShowDialog() == DialogResult.OK)
            {
                tabControl1.Enabled = false;
                LinkProgressAdd p = InitProgress();
                try
                {
                    p.TotalCount = dialog_directory.Links.Length;

                    ListViewItem[] buffer = new ListViewItem[Program.BufferCnt];
                    int j = 1;
                    for (int i = 0; i < dialog_directory.Links.Length; i++)
                    {
                        try
                        {
                            p.CurrentCount++;
                            p.Added++;
                            LinkDirectory link = dialog_directory.Links[i];
                            buffer[j - 1] = Make(link);

                            if (j == buffer.Length)
                            {
                                j = 0;
                                hsListView1.Items.AddRange(buffer);
                                await Task.Delay(1);
                            }
                            j++;
                        }
                        catch (Exception ex) { p.Fail++; }
                    }
                    for (int i = 0; i < j - 1; i++)
                    {
                        hsListView1.Items.Add(buffer[i]);
                        await Task.Delay(1);
                    }
                }
                finally { p.Done(true); }
            }
        }

        private void hsListView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (IsBusy) MessageBox.Show(LanguageManager.Language["STR_MAIN_MSG_RMVTASK"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int i = hsListView1.SelectedItems.Count - 1; i >= 0; i--)
                        hsListView1.SelectedItems[i].Remove();
                }
            }
        }

        private void btnClearComplete_Click(object sender, EventArgs e)
        {
            for (int i = hsListView1.Items.Count - 1; i >= 0; i--)
            {
                ProgressEventArgs pe = hsListView1.Items[i].SubItems[0].Tag as ProgressEventArgs;
                if (pe != null && pe.Result == Result.Success)
                    hsListView1.Items[i].Remove();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           //lblStatus.Text = string.Format("{0} / {1} ({2}%)", make.CurrentCount, make.TotalCount, make.Percent);
        }

        ListViewItem Find(Link link)
        {
            for(int i = 0; i < hsListView1.Items.Count; i++)
            {
                Link li = hsListView1.Items[i].Tag as Link;
                if (li != null && link.ID == li.ID) return hsListView1.Items[i];
            }
            return null;
        }
        void UpdateLanguage()
        {
            Text = LanguageManager.Language["STR_MAIN_TITLE"];
            btnMake.Text = LanguageManager.Language[btnMake.Tag.ToString()];
            lblStatus.Text = LanguageManager.Language[lblStatus.Tag.ToString()];

            FileFToolStripMenuItem.Text = LanguageManager.Language["STR_MAIN_MENU_FILE"];
            ExitXToolStripMenuItem.Text = LanguageManager.Language["STR_MAIN_MENU_EXIT"] + "(&X)";
            languageToolStripMenuItem.Text = LanguageManager.Language["STR_MAIN_MENU_LANG"];
            tabPage1.Text = LanguageManager.Language["STR_MAIN_TAB_MAIN"];
            tabPage2.Text = LanguageManager.Language["STR_MAIN_TAB_SETTINGS"];
            tabPage3.Text = LanguageManager.Language["STR_MAIN_TAB_LOG"];

            string head = LanguageManager.Language["STR_MAIN_TAB_ABOUT"];
            string tail = LanguageManager.Language["STR_MAIN_TAB_ABOUT_LINK"];
            aboutLinkLabel.Text = string.Format("{0} {1}", head, tail);
            aboutLinkLabel.LinkArea = new LinkArea(aboutLinkLabel.Text.Length - tail.Length, tail.Length);

            viewSourceCodeToolStripMenuItem.Text = LanguageManager.Language["STR_MAIN_MENU_GIT"];
            aboutToolStripMenuItem.Text = LanguageManager.Language["STR_MAIN_MENU_ABOUT"];
            //tail = LanguageManager.Language["STR_MAIN_TAB_ABOUT_GIT_LINK"];
            //int index = head.IndexOf("%1");
            //createdByLinkLabel.Text = head.Replace("%1", tail);
            //if (index > -1) createdByLinkLabel.LinkArea = new LinkArea(index, tail.Length);

            btnAddFile.Text = LanguageManager.Language["STR_MAIN_ADDFILE"];
            btnAddDirectory.Text = LanguageManager.Language["STR_MAIN_ADDDIR"];
            btnClearComplete.Text = LanguageManager.Language["STR_MAIN_CLRCOMPL"];
            chkAutoClear.Text = LanguageManager.Language["STR_MAIN_AUTOCLEAR"];
            btnMake.Text = LanguageManager.Language["STR_MAIN_CREATE"];
            hsListView1.Columns[0].Text = LanguageManager.Language["STR_MAIN_LIST_COL_RESULT"];
            hsListView1.Columns[1].Text = LanguageManager.Language["STR_MAIN_LIST_COL_NAME"];
            hsListView1.Columns[2].Text = LanguageManager.Language["STR_MAIN_LIST_COL_KIND"];
            hsListView1.Columns[3].Text = LanguageManager.Language["STR_MAIN_LIST_COL_METHOD"];
            hsListView1.Columns[4].Text = LanguageManager.Language["STR_MAIN_LIST_COL_PATH_DEST"];
            hsListView1.Columns[5].Text = LanguageManager.Language["STR_MAIN_LIST_COL_PATH_ORIG"];

            groupBox2.Text = LanguageManager.Language["STR_MAIN_SETTING_ADVOPTION"];
            chkStoreAttributes.Text = LanguageManager.Language["STR_MAIN_SETTING_ADVOPTION_STOREATTR"];
            chkStoreDate.Text = LanguageManager.Language["STR_MAIN_SETTING_ADVOPTION_STOREDATE"];
            groupBox3.Text = LanguageManager.Language["STR_MAIN_SETTING_LINKOPTION"];
            chkLinkReplace.Text = LanguageManager.Language["STR_MAIN_SETTING_LINKOPTION_REPLACE"];
            chkRegedit.Text = LanguageManager.Language["STR_MAIN_SETTING_REGEXP"];
            label1.Text = LanguageManager.Language["STR_MAIN_SETTING_REGEXP_NAME"];

            btnLogClear.Text = LanguageManager.Language["STR_MAIN_LOG_CLEAR"];
            chkLogWrap.Text = LanguageManager.Language["STR_MAIN_LOG_WORDWRAP"];
            chkLogScroll.Text = LanguageManager.Language["STR_MAIN_LOG_AUTOSCROL"];

            editToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_EDIT"];
            deleteToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_DELETE"];
            selectToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_SELECT"];
            selectAllToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_SELECT_ALL"];
            selectNoneToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_SELECT_NONE"];
            selectReverseToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_SELECT_REV"];
            checkToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_CHECK"];
            checkAllToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_CHECK_ALL"];
            checkNoneToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_CHECK_NONE"];
            checkReverseToolStripMenuItem.Text = LanguageManager.Language["STR_MENU_CHECK_REV"];

            for (int i = 0; i < hsListView1.Items.Count; i++)
            {
                ListViewItem li = hsListView1.Items[i];
                if (li.SubItems[0].Tag != null) li.SubItems[0].Text = LanguageManager.Language[li.SubItems[0].Tag.ToString()];
                Link link = li.Tag as Link;

                if (link != null)
                {
                    LinkDirectory linkd = li.Tag as LinkDirectory;
                    li.SubItems[2].Text = linkd == null ? LanguageManager.Language["STR_FILE"] : LanguageManager.Language["STR_DIR"];

                    if (link.Kind == LinkKind.Shortcut) li.SubItems[3].Text = LanguageManager.Language["STR_SHORTCUT"];
                    else if (link.Kind == LinkKind.SymLink) li.SubItems[3].Text = LanguageManager.Language["STR_SYMLINK"];
                    else
                    {
                        if (linkd == null) li.SubItems[3].Text = LanguageManager.Language["STR_HARDLINK"];
                        else 
                        {
                            if (linkd.IsOnlyFile) li.SubItems[3].Text = LanguageManager.Language["STR_HARDLINK"];
                            else li.SubItems[3].Text = LanguageManager.Language["STR_JUNCLINK"];
                        }
                    }

                    //JAVA Compatable
                    if(linkd != null ? linkd.IsOnlyFile : false) li.SubItems[3].Text += string.Format("({0})", linkd.Recursive ? "●" : "○");
                }
            }
        }

        Dictionary<string, string> Settings = new Dictionary<string, string>();
        string SettingsPath;
        bool LoadingSetting = false;
        void LoadSettings()
        {
            LoadingSetting = true;
            try
            {
                if (File.Exists(SettingsPath))
                {
                    string[] s = File.ReadAllLines(SettingsPath);
                    for (int i = 0; i < s.Length; i++)
                    {
                        int index = s[i].IndexOf('=');
                        if (index > 0)
                        {
                            string key = s[i].Remove(index);
                            string value = s[i].Substring(index + 1);
                            SetSettings(key, value);

                            if (key == "Language") try { toolStripComboBox1.SelectedIndex = int.Parse(value); } catch { }
                        }
                    }
                }
            }
            catch { }
            LoadingSetting = false;
        }
        void SaveSettings()
        {
            if (!LoadingSetting)
            {
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (KeyValuePair<string, string> value in Settings)
                        sb.Append(value.Key).Append("=").AppendLine(value.Value);

                    File.WriteAllText(SettingsPath, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(LanguageManager.Language["STR_MAIN_MSG_SAVESETTINGS"] + "\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        void SetSettings(string Name, string Contents)
        {
            if (Settings.ContainsKey(Name)) Settings[Name] = Contents;
            else Settings.Add(Name, Contents);
        }
        string GetSettings(string Name)
        {
            if (Settings.ContainsKey(Name)) return Settings[Name];
            else return null;
        }

        void Edit(int index, Link link) { Edit(hsListView1.Items[index], link); }
        void Edit(ListViewItem li, Link link)
        {
            li.Tag = link;
            li.SubItems[1].Text = Path.GetFileName(link.DestinationPath);
            li.SubItems[4].Text = link.DestinationPath;
            li.SubItems[5].Text = link.OriginalPath;

            if (link.Kind == LinkKind.Shortcut) li.SubItems[3].Text = LanguageManager.Language["STR_SHORTCUT"];
            else if (link.Kind == LinkKind.SymLink) li.SubItems[3].Text = LanguageManager.Language["STR_SYMLINK"];
            else
            {
                if (link is LinkFile)li.SubItems[3].Text = LanguageManager.Language["STR_HARDLINK"];
                else if(link is LinkDirectory)
                {
                    LinkDirectory linkd = (LinkDirectory)link;
                    if (linkd.IsOnlyFile) li.SubItems[3].Text = string.Format("{0} ({1})", LanguageManager.Language["STR_HARDLINK"], linkd.Recursive ? "●" : "○");
                    else li.SubItems[3].Text = LanguageManager.Language["STR_JUNCLINK"];
                }
            }
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetSettings("Language", toolStripComboBox1.SelectedIndex.ToString());
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<LinkDirectory> linkd = new List<LinkDirectory>();
            List<LinkFile> linkf = new List<LinkFile>();

            List<ListViewItem> itemd = new List<ListViewItem>();
            List<ListViewItem> itemf = new List<ListViewItem>();

            for (int i = 0; i < hsListView1.SelectedItems.Count; i++)
            {
                ListViewItem item = hsListView1.SelectedItems[i];
                LinkDirectory link = item.Tag as LinkDirectory;
                if (link != null) { linkd.Add(link); itemd.Add(item); }
                else { linkf.Add((LinkFile)item.Tag); itemf.Add(item); }
            }

            if (linkd.Count > 0)
            {
                frmEdit rename = new frmEdit(linkd.ToArray());
                if (rename.ShowDialog() == DialogResult.OK)
                    for (int i = 0; i < itemd.Count; i++) Edit(itemd[i], rename.Link[i] as LinkDirectory);
            }
            if (linkf.Count > 0)
            {
                frmEdit rename = new frmEdit(linkf.ToArray());
                if (rename.ShowDialog() == DialogResult.OK)
                    for (int i = 0; i < itemf.Count; i++) Edit(itemf[i], rename.Link[i] as LinkFile);
            }
            itemd.Clear();
            itemf.Clear();
            linkd.Clear();
            linkf.Clear();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = hsListView1.SelectedItems.Count - 1; i >= 0; i--)
                hsListView1.SelectedItems[i].Remove();
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            deleteToolStripMenuItem.Enabled = hsListView1.SelectedItems.Count > 0;
            editToolStripMenuItem.Enabled = hsListView1.SelectedItems.Count == 1;
            selectToolStripMenuItem.Enabled = checkToolStripMenuItem.Enabled = hsListView1.Items.Count > 0;
        }
    }
}
