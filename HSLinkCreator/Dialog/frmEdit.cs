﻿using HSLinkCreator.Core;
using HSLinkCreator.Language;
using Ionic.Utils;
using System;
using System.Windows.Forms;

namespace HSLinkCreator
{
    public partial class frmEdit : Form
    {
        public frmEdit(params LinkDirectory[] link)
        {
            int index = Init(link);
            this.Text = LanguageManager.Language["STR_FRM_EDIT_TITLE"].Replace("%s", LanguageManager.Language["STR_DIR"]);
            txtDestPath.Select(index, txtDestPath.Text.Length - index);

            panel1.Visible = true;
            CheckState[] state = GetCheckState(link);
            chkFileOnly.CheckState = state[0];
            chkRecursive.CheckState = state[1];
            chkFileOnly_CheckedChanged(this, null);

            txtOrigPath.Enabled = btnOriginal.Enabled = link.Length == 1;
        }
        public frmEdit(params LinkFile[] link)
        {
            int index = Init(link);
            this.Text = LanguageManager.Language["STR_FRM_EDIT_TITLE"].Replace("%s", LanguageManager.Language["STR_FILE"]);
            this.comboBox1.Items[0] = LanguageManager.Language["STR_SHORTCUT"];
            this.comboBox1.Items[1] = LanguageManager.Language["STR_SYMLINK"];
            this.comboBox1.Items[2] = LanguageManager.Language["STR_HARDLINK"];

            int index1 = txtDestPath.Text.LastIndexOf(".");
            int index2 = index1 - index;
            txtDestPath.Select(index, index2 > 0 ? index2 : txtDestPath.Text.Length - index1);

            txtOrigPath.Enabled = btnOriginal.Enabled = link.Length == 1;
        }
        bool SpecialLinkAvail = false;
        int Init(params Link[] link)
        {
            InitializeComponent();

            this.Link = link;
            if (link.Length > 1)
            {
                txtOrigPath.Enabled = btnOriginal.Enabled = false;
                txtDestPath.Text = link[0].DestinationPath;

                string drive = Utils.GetDrive(link[0].DestinationPath);
                string dir = Utils.GetDirectory(link[0].DestinationPath);
                for(int i = 1; i < link.Length; i++)
                {
                    string drive1 = Utils.GetDrive(link[i].DestinationPath);
                    if (!drive.ToUpper().Equals(drive1.ToUpper())) SpecialLinkAvail = false;
                    else drive = drive1;
                    
                    string dir1 = Utils.GetDirectory(link[i].DestinationPath);
                    if (!dir.ToUpper().Equals(dir.ToUpper())) txtDestPath.Text = null;//txtDestPath.Enabled = btnDestination.Enabled = false; 
                    else dir = dir1;
                }
            }
            else
            {
                txtOrigPath.Text = link[0].OriginalPath;
                txtDestPath.Text = link[0].DestinationPath;
                comboBox1.SelectedIndex = (int)link[0].Kind;
            }

            chkRecursive.Enabled = chkFileOnly.Checked;
            chkFileOnly.CheckedChanged += (o, e) => chkRecursive.Enabled = chkFileOnly.Checked;

            this.label1.Text = LanguageManager.Language["STR_FRM_TAB_ORIG"];
            this.label2.Text = LanguageManager.Language["STR_FRM_TAB_DEST"];
            this.label3.Text = LanguageManager.Language["STR_FRM_LBL_TYPE"];
            this.chkFileOnly.Text = LanguageManager.Language["STR_FRM_LINKDIR_CHK_OF"];
            this.chkRecursive.Text = LanguageManager.Language["STR_FRM_LINKDIR_CHK_RECUR"];
            this.Text = LanguageManager.Language["STR_MENU_EDIT"];
            this.button1.Text = LanguageManager.Language["STR_BTN_OK"];

            int index = txtOrigPath.Text.LastIndexOf("\\") + 1;
            txtOrigPath.Select(index, txtOrigPath.Text.Length - index);
            index = txtDestPath.Text.LastIndexOf("\\") + 1;
            return index;
        }
        CheckState[] GetCheckState(params LinkDirectory[] link)
        {
            bool Break_OF = false, Bread_Rec = false;
            bool OF = link[0].IsOnlyFile, Rec = link[0].Recursive;
            for(int i = 1; i < link.Length; i++)
            {
                if (OF != link[i].IsOnlyFile && !Break_OF) Break_OF = true;
                if (Rec != link[i].Recursive && !Bread_Rec) Bread_Rec = true;
                if (Break_OF && Bread_Rec) break;
            }

            CheckState[] state = new CheckState[2];
            if (Break_OF) state[0] = CheckState.Indeterminate;
            else state[0] = OF ? CheckState.Checked : CheckState.Unchecked;
            if (Bread_Rec) state[1] = CheckState.Indeterminate;
            else state[1] = Rec ? CheckState.Checked : CheckState.Unchecked;
            return state;
        }

        public Link[] Link { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtOrigPath.Text.Trim()) || string.IsNullOrEmpty(txtDestPath.Text.Trim()))
                MessageBox.Show(LanguageManager.Language["STR_FRM_LINKFILE_DLG_NOTEMPTY"], this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (Link.Length == 1)
                {
                    Link[0].OriginalPath = txtOrigPath.Text.Trim();
                    Link[0].DestinationPath = txtDestPath.Text.Trim();
                    Link[0].Kind = (LinkKind)comboBox1.SelectedIndex;

                    LinkDirectory linkd = Link[0] as LinkDirectory;
                    if (linkd != null)
                    {
                        linkd.IsOnlyFile = chkFileOnly.Checked;
                        linkd.Recursive = chkRecursive.Checked;
                    }
                }
                else
                {
                    bool a = string.IsNullOrEmpty(txtDestPath.Text),
                         a1 = chkFileOnly.CheckState != CheckState.Indeterminate,
                         a2 = chkRecursive.CheckState != CheckState.Indeterminate;
                    for (int i = 0; i < Link.Length; i++)
                    {
                        if (!a)
                        {
                            string name = Utils.GetFileName(Link[i].DestinationPath);
                            string dir = Utils.GetDirectory(Link[i].DestinationPath);
                            Link[i].DestinationPath = dir + name;
                        }

                        if (a1 || a2)
                        {
                            LinkDirectory linkd = Link[i] as LinkDirectory;
                            if (linkd != null)
                            {
                                if (chkFileOnly.CheckState != CheckState.Indeterminate) linkd.IsOnlyFile = chkFileOnly.Checked;
                                if (chkRecursive.CheckState != CheckState.Indeterminate) linkd.Recursive = chkRecursive.Checked;
                            }
                        }
                    }
                }

                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return || e.KeyCode == Keys.Enter) button1_Click(sender, e);
        }

        private void chkFileOnly_CheckedChanged(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            comboBox1.Items.Clear();
            if (chkFileOnly.CheckState == CheckState.Checked)
            {
                comboBox1.Items.AddRange(new string[] {
                    LanguageManager.Language["STR_SHORTCUT"],
                    LanguageManager.Language["STR_SYMLINK"],
                    LanguageManager.Language["STR_HARDLINK"]});
            }
            else if(chkFileOnly.CheckState == CheckState.Unchecked)
            {
                comboBox1.Items.AddRange(new string[] {
                    LanguageManager.Language["STR_SHORTCUT"],
                    LanguageManager.Language["STR_SYMLINK"],
                    LanguageManager.Language["STR_JUNCLINK"]});
            }
            else
            {
                comboBox1.Items.AddRange(new string[] {
                    LanguageManager.Language["STR_NOCHANGE"],
                    LanguageManager.Language["STR_SHORTCUT"],
                    LanguageManager.Language["STR_SYMLINK"]});
            }
            comboBox1.SelectedIndex = index < 0 ? 1 : index;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Orig = txtOrigPath.Text.Remove(txtOrigPath.Text.IndexOf("\\"));
            string Dest = txtDestPath.Text.Remove(txtDestPath.Text.IndexOf("\\"));
            if (comboBox1.Items.Count == 3 && comboBox1.SelectedIndex == 2 && Orig.ToUpper() != Dest.ToUpper())
            {
                comboBox1.SelectedIndex = 1;
                string msg = Link[0] is LinkFile ? LanguageManager.Language["STR_MSG_HARDLINK_WARN"] : LanguageManager.Language["STR_MSG_JUNCTION_WARN"];
                MessageBox.Show(msg, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void frmEdit_Load(object sender, EventArgs e)
        {
        }

        private void btnOriginal_Click(object sender, EventArgs e)
        {
            if (Link[0] is LinkDirectory)
            {
                FolderBrowserDialogEx dialog = FolderBrowserDialogEx.LocalBrowser();
                dialog.SelectedPath = txtOrigPath.Text;
                if (dialog.ShowDialog() == DialogResult.OK) txtOrigPath.Text = dialog.SelectedPath;
            }
            else
            {
                OpenFileDialog dialog = new OpenFileDialog()
                {
                    Multiselect = true,
                    DefaultExt = "*.*",
                    Filter = LanguageManager.Language["STR_FRM_LINKFILE_TSM_EXT_ALL"] + "|*.*"
                };
                dialog.FileName = txtOrigPath.Text;
                if (dialog.ShowDialog() == DialogResult.OK) txtOrigPath.Text = dialog.FileName;
            }
        }

        private void btnDestination_Click(object sender, EventArgs e)
        {
            if (Link.Length > 1)
            {
                //폴더 다이얼로그 소환
                if (Link[0] is LinkDirectory)
                {

                }
                else
                {

                }
            }
            else
            {
                if (Link[0] is LinkDirectory)
                {

                }
                else
                {

                }
            }
        }
    }
}
