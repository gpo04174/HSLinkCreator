﻿using System;
using System.Collections.Generic;

namespace HSLinkCreator.Language
{
    public interface IUpdateLanguage { void UpdateLanguage(); }
    public class LanguageManager
    {
        static LanguageManager _Language = GetDefault();
        public static LanguageManager Language
        {
            get { return _Language; }
            set
            {
                _Language = value;
                if (LanguageChanged != null) try { LanguageChanged.Invoke(_Language, new EventArgs()); } catch { }
            }
        }
        public static event EventHandler LanguageChanged;

        //public static void OnLanguageChanged(LanguageManager sender) { if (LanguageChanged != null) try { LanguageChanged.Invoke(sender, new EventArgs()); } catch { } }


        Dictionary<string, string> lng = new Dictionary<string, string>();
        public LanguageManager(string Name = "English") { this.Name = Name; }
        public LanguageManager(string[] Language, string Name)
        {
            this.Name = Name;
            for (int i = 0; i < Language.Length; i++) AddLanguage(Language[i]);
        }
        public LanguageManager(string Language, string Name)
        {
            this.Name = Name;
            int LastIndex = 0;
            string line = null;
            for (int i = 0; i < Language.Length; i++)
                if ((Language[i] == '\n' || Language[i] == '\r') && i - LastIndex > 1)
                {
                    line = Language.Substring(LastIndex, i - LastIndex).Trim();
                    AddLanguage(line);
                    LastIndex = i;
                }
            line = Language.Substring(LastIndex, Language.Length - LastIndex).Trim();
            AddLanguage(line);
        }
        void AddLanguage(string line)
        {
            if (!string.IsNullOrEmpty(line) &&
                (line[0] >= 'A' && line[0] <= 'Z'))
            {
                int index = line.IndexOf('=');
                if (index > 0)
                {
                    string key = line.Remove(index);
                    if (!Exist(key)) lng.Add(key, line.Substring(index + 1).Replace("\\n", "\n"));
                }
            }
        }

        public string this[string Key] { get { return lng.ContainsKey(Key) ? lng[Key] : Key; } }
        public bool Exist(string Key) { return lng.ContainsKey(Key); }
        public int Count { get { return lng.Count; } }

        public string Name { get; set; }

        //Language: English (Defalut)
        //Author: HongSic(HSKernel)
        public static LanguageManager GetDefault()
        {
            LanguageManager m = new LanguageManager();
            //m.lng.Add("Language", "English");
            //Main Area
            m.lng.Add("STR_MAIN_TITLE", "HS Link Creator");
            m.lng.Add("STR_MAIN_MENU_FILE", "File");
            m.lng.Add("STR_MAIN_MENU_EXIT", "Exit");
            m.lng.Add("STR_MAIN_MENU_LANG", "Language");
            m.lng.Add("STR_MAIN_MENU_ABOUT", "About");
            m.lng.Add("STR_MAIN_MENU_GIT", "View source code");
            m.lng.Add("STR_FILE", "File");
            m.lng.Add("STR_DIR", "Directory(Folder)");
            m.lng.Add("STR_SHORTCUT", "Shortcut");
            m.lng.Add("STR_SYMLINK", "Symbolic Link");
            m.lng.Add("STR_HARDLINK", "Hard Link");
            m.lng.Add("STR_JUNCLINK", "Junction Link");
            m.lng.Add("STR_NOCHANGE", "No Change");
            //m.lng.Add("STR_SHORTCUT_FO", "Shortcut (Files only)");
            //m.lng.Add("STR_SYMLINK_FO", "Symbolic Link (Files only)");
            //m.lng.Add("STR_HARDLINK_FO", "Hard Link (Files only)");
            m.lng.Add("STR_BTN_OK", "OK");
            m.lng.Add("STR_BTN_CANCEL", "Cancel");
            m.lng.Add("STR_BTN_YES", "Yes");
            m.lng.Add("STR_BTN_NO", "No");
            m.lng.Add("STR_MSG_HARDLINK_WARN", "Hard links can only be on the same drive and are replaced by Symbolic Links when the drives are different.");
            m.lng.Add("STR_MSG_JUNCLINK_WARN", "Directory (folder) Junction can only have the same drive, and will automatically replace it with a Symbolic Link when the drive is different.");

            m.lng.Add("STR_RESULT_SUCCESS", "Success");
            m.lng.Add("STR_RESULT_FAIL", "Fail");
            m.lng.Add("STR_RESULT_STARTING", "Starting");
            m.lng.Add("STR_RESULT_UNKNOWN", "Unknown");

            m.lng.Add("STR_MENU_EDIT", "Edit");
            m.lng.Add("STR_MENU_DELETE", "Delete");
            m.lng.Add("STR_MENU_SELECT", "Select");
            m.lng.Add("STR_MENU_SELECT_ALL", "Select All");
            m.lng.Add("STR_MENU_SELECT_NONE", "Select None");
            m.lng.Add("STR_MENU_SELECT_REV", "Select Reverse");
            m.lng.Add("STR_MENU_CHECK", "Check");
            m.lng.Add("STR_MENU_CHECK_ALL", "Check All");
            m.lng.Add("STR_MENU_CHECK_NONE", "Check None");
            m.lng.Add("STR_MENU_CHECK_REV", "Check Reverse");


            m.lng.Add("STR_MAIN_TAB_MAIN", "Main");
            m.lng.Add("STR_MAIN_TAB_SETTINGS", "Settings");
            m.lng.Add("STR_MAIN_TAB_LOG", "Log");
            m.lng.Add("STR_MAIN_TAB_ABOUT", "HSLinkManager creates multiple types of symbolic links.");
            m.lng.Add("STR_MAIN_TAB_ABOUT_LINK", "Learn More");
            //m.lng.Add("STR_MAIN_TAB_ABOUT_GIT", "Source code on %1");
            //m.lng.Add("STR_MAIN_TAB_ABOUT_GIT_LINK", "GItLab");

            m.lng.Add("STR_MAIN_ADDFILE", "Add Files");
            m.lng.Add("STR_MAIN_ADDDIR", "Add Directory(Folder)");
            m.lng.Add("STR_MAIN_CLRCOMPL", "Clear Complete");
            m.lng.Add("STR_MAIN_AUTOCLEAR", "Auto Clear");
            m.lng.Add("STR_MAIN_CREATE", "Create");
            m.lng.Add("STR_MAIN_ABORT", "Abort");
            m.lng.Add("STR_MAIN_LIST_COL_RESULT", "Result");
            m.lng.Add("STR_MAIN_LIST_COL_NAME", "Name");
            m.lng.Add("STR_MAIN_LIST_COL_KIND", "Kind");
            m.lng.Add("STR_MAIN_LIST_COL_METHOD", "Method");
            m.lng.Add("STR_MAIN_LIST_COL_PATH_ORIG", "Original Path");
            m.lng.Add("STR_MAIN_LIST_COL_PATH_DEST", "Destination(Link) Path");

            m.lng.Add("STR_MAIN_SETTING_ADVOPTION", "Advance Option");
            m.lng.Add("STR_MAIN_SETTING_ADVOPTION_STOREATTR", "Store Attributes");
            m.lng.Add("STR_MAIN_SETTING_ADVOPTION_STOREDATE", "Store Date");
            m.lng.Add("STR_MAIN_SETTING_LINKOPTION", "Link Option");
            m.lng.Add("STR_MAIN_SETTING_LINKOPTION_REPLACE", "Replace if item exist");
            m.lng.Add("STR_MAIN_SETTING_REGEXP", "Register to Explorer");
            m.lng.Add("STR_MAIN_SETTING_REGEXP_NAME", "Name");

            m.lng.Add("STR_MAIN_LOG_CLEAR", "Clear Log");
            m.lng.Add("STR_MAIN_LOG_WORDWRAP", "Word Wrap");
            m.lng.Add("STR_MAIN_LOG_AUTOSCROL", "Auto Scroll");

            m.lng.Add("STR_MAIN_STAT_READY", "Ready");
            m.lng.Add("STR_MAIN_STAT_START", "Preparing to start...");
            m.lng.Add("STR_MAIN_STAT_COMPLETE", "Complete");
            m.lng.Add("STR_MAIN_STAT_ABORT", "Aborted");

            m.lng.Add("STR_MAIN_MSG_ADDITEM", "Add items first.");
            m.lng.Add("STR_MAIN_MSG_RMVTASK", "Can't delete items when task is running");
            m.lng.Add("STR_MAIN_MSG_SAVESETTINGS", "An error has occured when saving settings");
            m.lng.Add("STR_MAIN_MSG_", "");

            m.lng.Add("STR_CORE_MSG_LINK_OK", "Link created");
            m.lng.Add("STR_CORE_MSG_LINK_FAIL", "Link creation failed");
            m.lng.Add("STR_CORE_MSG_LINK_START", "Link creation start...");
            m.lng.Add("STR_CORE_MSG_LINK_UNKNOWN", "Unknown result (Link)");
            m.lng.Add("STR_CORE_MSG_LINK_NOTFOUND", "Can't find file or folder (directory).");
            m.lng.Add("STR_CORE_MSG_DELETE_OK", "Delete already existing items");
            m.lng.Add("STR_CORE_MSG_DELETE_FAIL", "There was an error deleting destnation %1");
            m.lng.Add("STR_CORE_MSG_DELETE_START", "Delete start...");
            m.lng.Add("STR_CORE_MSG_DELETE_UNKNOWN", "Unknown result (Delete)");
            m.lng.Add("STR_CORE_MSG_ATTR_OK", "Attribute applied");
            m.lng.Add("STR_CORE_MSG_ATTR_FAIL", "Attribute is not applied");
            m.lng.Add("STR_CORE_MSG_ATTR_START", "Attribute apply start...");
            m.lng.Add("STR_CORE_MSG_ATTR_UNKNOWN", "Unknown result (Attribute)");
            m.lng.Add("STR_CORE_MSG_DATE_CREATE_OK", "Date applied (Creation Time)");
            m.lng.Add("STR_CORE_MSG_DATE_CREATE_FAIL", "Date is not applied (Creation Time)");
            m.lng.Add("STR_CORE_MSG_DATE_ACCTIME_OK", "Date applied (Last Access Time)");
            m.lng.Add("STR_CORE_MSG_DATE_ACCTIME_FAIL", "Date is not applied (Last Access Time)");
            m.lng.Add("STR_CORE_MSG_DATE_WRITE_OK", "Date applied (Last Write Time)");
            m.lng.Add("STR_CORE_MSG_DATE_WRITE_FAIL", "Date is not applied (Last Write Time)");
            m.lng.Add("STR_CORE_MSG_DATE_", "");

            m.lng.Add("STR_FRM_LBL_PATH", "Link Path");
            m.lng.Add("STR_FRM_LBL_TYPE", "Link Type");
            m.lng.Add("STR_FRM_BTN_ADD", "Add");
            m.lng.Add("STR_FRM_BTN_CLRALL", "Clear All");
            m.lng.Add("STR_FRM_TAB_NAME", "Name");
            m.lng.Add("STR_FRM_TAB_TYPE", "Type");
            m.lng.Add("STR_FRM_TAB_DEST", "Link Path");
            m.lng.Add("STR_FRM_TAB_ORIG", "Original Path");
            m.lng.Add("STR_FRM_TAB_OF", "O.F");

            m.lng.Add("STR_FRM_LINKFILE_TITLE", "Create to file link");
            m.lng.Add("STR_FRM_LINKFILE_TSM_EXT_ALL", "All Files");
            m.lng.Add("STR_FRM_LINKFILE_DLG_NOTEMPTY", "Orignal or Destination path is can not empty");
            m.lng.Add("STR_FRM_LINKFILE_MSG_DELETEALL", "Are you sure to delete all items?");
            m.lng.Add("STR_FRM_LINKFILE_DLG_FILEOPEN", "Please select file to link create");
            m.lng.Add("STR_FRM_LINKFILE_DLG_DIROPEN", "Please select directory to link create first.");

            m.lng.Add("STR_FRM_LINKDIR_TITLE", "Create to directory(folder) link");
            m.lng.Add("STR_FRM_LINKDIR_CHK_OF", "Only Files");
            m.lng.Add("STR_FRM_LINKDIR_CHK_RECUR", "Recursive");
            m.lng.Add("STR_FRM_LINKDIR_BTN_CLRALL", "Clear All");

            m.lng.Add("STR_FRM_LINKDIR_CHK_RECUL_DESC", "Whether recursive to subdirectory");
            m.lng.Add("STR_FRM_LINKDIR_MSG_CLERALL", "Are you sure to delete all items?");
            m.lng.Add("STR_FRM_LINKDIR_DLG_ORIG", "Please select directory to link create");
            m.lng.Add("STR_FRM_LINKDIR_DLG_DEST", "Please select to directory(floder)");
            m.lng.Add("STR_FRM_LINKDIR_", "");

            m.lng.Add("STR_FRM_EDIT_TITLE", "Edit Link (%s)");

            m.lng.Add("STR_FRM_PROGRESS_ADDING", "Adding...");
            m.lng.Add("STR_FRM_PROGRESS_RESULT", "Result");
            m.lng.Add("STR_FRM_PROGRESS_ADD", "Add");
            m.lng.Add("STR_FRM_PROGRESS_FAIL", "Fail");
            m.lng.Add("STR_FRM_PROGRESS_PASS", "Pass");
            m.lng.Add("STR_FRM_PROGRESS_TOTAL", "Total %s");

            return m;
        }
    }
}
