﻿using HSLinkCreator.Language;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HSLinkCreator.Core
{
    public class MakeLinkEXE : MakeLink
    {
        public MakeLinkEXE(Control UIControl = null)
        {
            base.UIControl = UIControl;
            start = Work;
        }
        public override void Start(params Link[] Job)
        {
            TotalCount = Job.Length;
            try { if (thread != null) thread.Abort(); } catch { }
            thread = new Thread(start);
            thread.Start(Job);
        }
        public override void Stop()
        {
            try { if (thread != null) thread.Abort(); } catch { }
            //OnComplete(this, DoneResult.Cancel);
        }

        public void Work(object o)
        {
            Link[] Job = o as Link[];
            try
            {
                ProgressEventArgs[] result = new ProgressEventArgs[Job.Length];
                for (int i = 0; i < Job.Length; i++)
                {
                    Current = Job[i];
                    OnProgressing(this, Current);
                    result[i] = Process(Current);
                    OnProgressed(this, result[i]);
                    CurrentCount = i + 1;
                }

                OnComplete(this, DoneResult.Complete);
            }
            catch (ThreadAbortException) { OnComplete(this, DoneResult.Cancel); }
            catch { OnComplete(this, DoneResult.Cancel_Fail); }
        }

        #region Core
        public static Result GetMakeResult(LoggingEventArgs[] Result)
        {
            for(int i = 0; i < Result.Length; i++)
            {
                if (Result[i].Kind == ProgressKind.Create)
                    return Result[i].Result;
            }
            return Core.Result.Unknown;
        }
        public static LoggingEventArgs[] Make(Link link, bool LinkReplace, bool ApplyDate, bool ApplyProperty)
        {
            List<LoggingEventArgs> log = new List<LoggingEventArgs>();
            if (LinkReplace)
            {
                try
                {
                    if (File.Exists(link.DestinationPath)) File.Delete(link.DestinationPath);
                    else if (Directory.Exists(link.DestinationPath)) Directory.Delete(link.DestinationPath);
                    log.Add(new LoggingEventArgs(link, ProgressKind.Delete, Result.Success));
                }
                catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Delete, Result.Fail, ex.Message)); }
            }

            log.Add(new LoggingEventArgs(link, ProgressKind.Create, Result.Starting));

            ProgressEventArgs result = null;
            if (link.Kind == LinkKind.Shortcut) result = MakeShortcut(link);
            else result = MakeLink(link);
            log.Add(new LoggingEventArgs(ProgressKind.Create, result));

            if (ApplyDate || ApplyProperty)
            {
                if (Directory.Exists(link.DestinationPath))
                {
                    if (ApplyProperty)
                    {
                        try
                        {
                            DirectoryInfo di = new DirectoryInfo(link.DestinationPath);
                            DirectoryInfo di1 = new DirectoryInfo(link.OriginalPath);
                            di.Attributes = di1.Attributes;
                            log.Add(new LoggingEventArgs(link, ProgressKind.Attribute, Result.Success));
                        }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Attribute, Result.Fail, ex.Message)); }
                    }
                    if (ApplyDate)
                    {
                        try { Directory.SetCreationTime(link.DestinationPath, Directory.GetCreationTime(link.OriginalPath)); log.Add(new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Success)); }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Fail, ex.Message)); }
                        try { Directory.SetLastAccessTime(link.DestinationPath, Directory.GetLastAccessTime(link.OriginalPath)); log.Add(new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Success)); }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Fail, ex.Message)); }
                        try { Directory.SetLastWriteTime(link.DestinationPath, Directory.GetLastWriteTime(link.OriginalPath)); log.Add(new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Success)); }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Fail, ex.Message)); }
                    }
                }
                else if (File.Exists(link.DestinationPath))
                {
                    if (ApplyProperty)
                    {
                        try
                        {
                            File.SetAttributes(link.DestinationPath, File.GetAttributes(link.OriginalPath));
                            log.Add(new LoggingEventArgs(link, ProgressKind.Attribute, Result.Success));
                        }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Attribute, Result.Fail, ex.Message)); }
                    }
                    if (ApplyDate)
                    {
                        try { Directory.SetCreationTime(link.DestinationPath, Directory.GetCreationTime(link.OriginalPath)); log.Add(new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Success)); }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Fail, ex.Message)); }
                        try { Directory.SetLastAccessTime(link.DestinationPath, Directory.GetLastAccessTime(link.OriginalPath)); log.Add(new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Success)); }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Fail, ex.Message)); }
                        try { Directory.SetLastWriteTime(link.DestinationPath, Directory.GetLastWriteTime(link.OriginalPath)); log.Add(new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Success)); }
                        catch (Exception ex) { log.Add(new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Fail, ex.Message)); }
                    }
                }
            }
            return log.ToArray();
        }
        
        private ProgressEventArgs Process(Link link)
        {
            OnLogging(this, new LoggingEventArgs(link, ProgressKind.Create, Result.Starting));
            if (LinkReplace)
            {
                try
                {
                    if (File.Exists(link.DestinationPath))
                    {
                        File.Delete(link.DestinationPath);
                        OnLogging(this, new LoggingEventArgs(link, ProgressKind.Delete, Result.Success));
                    }
                    else if (Directory.Exists(link.DestinationPath))
                    {
                        Directory.Delete(link.DestinationPath);
                        OnLogging(this, new LoggingEventArgs(link, ProgressKind.Delete, Result.Success));
                    }
                }
                catch (Exception ex){ OnLogging(this, new LoggingEventArgs(link, ProgressKind.Delete, Result.Fail, ex.Message)); }
            }

            ProgressEventArgs result = null;
            if (link.Kind == LinkKind.Shortcut) result = MakeShortcut(link);
            else result = MakeLink(link);
            OnLogging(this,　new LoggingEventArgs(ProgressKind.Create, result));

            if (ApplyDate || ApplyProperty)
            {
                if (Directory.Exists(link.DestinationPath))
                {
                    if (ApplyProperty)
                    {
                        try
                        {
                            DirectoryInfo di = new DirectoryInfo(link.DestinationPath);
                            DirectoryInfo di1 = new DirectoryInfo(link.OriginalPath);
                            di.Attributes = di1.Attributes;
                            OnLogging(this, new LoggingEventArgs(link, ProgressKind.Attribute, Result.Success));
                        }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Attribute, Result.Fail, ex.Message)); }
                    }
                    if (ApplyDate)
                    {
                        try { Directory.SetCreationTime(link.DestinationPath, Directory.GetCreationTime(link.OriginalPath)); OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Success)); }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Fail, ex.Message)); }
                        try { Directory.SetLastAccessTime(link.DestinationPath, Directory.GetLastAccessTime(link.OriginalPath)); OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Success)); }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Fail, ex.Message)); }
                        try { Directory.SetLastWriteTime(link.DestinationPath, Directory.GetLastWriteTime(link.OriginalPath)); OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Success)); }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Fail, ex.Message)); }
                    }
                }
                else if (File.Exists(link.DestinationPath))
                {
                    if (ApplyProperty)
                    {
                        try
                        {
                            File.SetAttributes(link.DestinationPath, File.GetAttributes(link.OriginalPath));
                            OnLogging(this, new LoggingEventArgs(link, ProgressKind.Attribute, Result.Success));
                        }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Attribute, Result.Fail, ex.Message)); }
                    }
                    if (ApplyDate)
                    {
                        try { File.SetCreationTime(link.DestinationPath, File.GetCreationTime(link.OriginalPath)); OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Success)); }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Create, Result.Fail, ex.Message)); }
                        try { File.SetLastAccessTime(link.DestinationPath, File.GetLastAccessTime(link.OriginalPath)); OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Success)); }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_Access, Result.Fail, ex.Message)); }
                        try { File.SetLastWriteTime(link.DestinationPath, File.GetLastWriteTime(link.OriginalPath)); OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Success)); }
                        catch (Exception ex) { OnLogging(this, new LoggingEventArgs(link, ProgressKind.Time_LastWrite, Result.Fail, ex.Message)); }
                    }
                }
            }
            return result;
        }
        private static ProgressEventArgs MakeLink(Link link)
        {
            string args = null;
            if (Directory.Exists(link.OriginalPath))
            {
                string option = link.Kind == LinkKind.Special ? "/j" : "";
                args = string.Format("/c mklink /d {0} \"{1}\" \"{2}\"", option, link.DestinationPath, link.OriginalPath);
            }
            else if (File.Exists(link.OriginalPath)) args = string.Format("/c mklink {0} \"{1}\" \"{2}\"", link.Kind == LinkKind.Special ? "/h " : "", link.DestinationPath, link.OriginalPath);
            else return new ProgressEventArgs(link, Result.Fail, LanguageManager.Language["STR_CORE_MSG_LINK_NOTFOUND"]); 

            Process process = new Process();
            ProcessStartInfo info = new ProcessStartInfo("cmd")
            {
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                CreateNoWindow = true,
                Arguments = args
            };
            process.StartInfo = info;
            process.Start();
            StreamReader standardOutput = process.StandardOutput;
            //StreamWriter standardInput = process.StandardInput;
            //standardInput.Flush();
            //standardInput.Close();
            string msg = standardOutput.ReadToEnd().Trim();
            process.WaitForExit(2000);

            if (process.ExitCode == 0) return new ProgressEventArgs(link, Result.Success, msg);
            else if (process.ExitCode == 1) return new ProgressEventArgs(link, Result.Fail, msg);
            else return new ProgressEventArgs(link, (Result)process.ExitCode, msg);

            //process.WaitForExit(2000);
        }
        private static ProgressEventArgs MakeShortcut(Link link)
        {
            try
            {
                IShellLink shell = (IShellLink) new ShellLink();

                // setup shortcut information
                shell.SetDescription(Path.GetFileNameWithoutExtension(link.OriginalPath));
                shell.SetPath(link.OriginalPath);
                
                // save it
                IPersistFile file = (IPersistFile)shell;
                File.Create(link.DestinationPath + ".lnk").Close();
                file.Save(link.DestinationPath + ".lnk", false);

                return new ProgressEventArgs(link, Result.Success);
            }
            catch (Exception ex) { return new ProgressEventArgs(link, Result.Fail, ex.Message); }
        }

        [ComImport]
        [Guid("00021401-0000-0000-C000-000000000046")]
        internal class ShellLink {}

        [ComImport]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("000214F9-0000-0000-C000-000000000046")]
        internal interface IShellLink
        {
            void GetPath([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile, int cchMaxPath, out IntPtr pfd, int fFlags);
            void GetIDList(out IntPtr ppidl);
            void SetIDList(IntPtr pidl);
            void GetDescription([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszName, int cchMaxName);
            void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
            void GetWorkingDirectory([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir, int cchMaxPath);
            void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
            void GetArguments([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs, int cchMaxPath);
            void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
            void GetHotkey(out short pwHotkey);
            void SetHotkey(short wHotkey);
            void GetShowCmd(out int piShowCmd);
            void SetShowCmd(int iShowCmd);
            void GetIconLocation([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszIconPath, int cchIconPath, out int piIcon);
            void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
            void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, int dwReserved);
            void Resolve(IntPtr hwnd, int fFlags);
            void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
        }


        [Obsolete]
        public string LnkToFile(string fileLink)
        {
            string link = File.ReadAllText(fileLink);
            int i1 = link.IndexOf("DATA\0");
            if (i1 < 0)
                return null;
            i1 += 5;
            int i2 = link.IndexOf("\0", i1);
            if (i2 < 0)
                return link.Substring(i1);
            else
                return link.Substring(i1, i2 - i1);
        }
        #endregion
    }
}
